# Cherry pie

A condensed bitmap font based on [cherry](https://github.com/turquoise-hexagon/cherry), [curie](https://github.com/NerdyPepper/curie) and [scientifica](https://github.com/NerdyPepper/scientifica)

## Features

- Blocky bold variant;
- Condensed;
- Powerline glyphs and some other unicode symbols (more to come);
- Looks nice (I think...).

## Installation

```
$ git clone https://gitlab.com/heyitscassio/cherrypie.git
$ cd cherrypie
$ cp *.bdf ~/.local/share/fonts
```

## Screenshots

![img1](img/img2.png)
